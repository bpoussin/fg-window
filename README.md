Extension de navigateur pour mettre en avant plan la fenetre en fonction du titre.
Lorsque le titre change, l'extension vérifie s'il faut mettre en avant plan la fenetre.

Installation: pour l'instant il faut récupérer ce repo, se mettre en mode developpeur
et ajouter l'extension non packagé.

L'usage initiale est de mettre en avant plan les fenêtres riot/element
lorsqu'on est mentionné.

exemple de pattern pour
- riot/element: Riot \[\d+\]
- teams: \(\d+\).*\| Microsoft 
