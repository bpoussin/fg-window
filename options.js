function saveOptions(e) {
  e.preventDefault();
  chrome.storage.sync.set({
    regexp: document.querySelector("#regexp").value
  });
}

function restoreOptions() {
  chrome.storage.sync.get('regexp', res => {
    document.querySelector("#regexp").value = res.regexp || '';
  });
}

document.addEventListener('DOMContentLoaded', restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);
