chrome.tabs.onUpdated.addListener(( tabId, info, tab) => {
  console.log("fg-window", info.title)
  if (info.title) {
    let getting = chrome.storage.sync.get("regexp", conf => {
      if (conf.regexp) {
        const regexp = "(" + conf.regexp.trim().split("\n").join(")|(") + ")"
        console.log("regexp", regexp);
        if (conf && conf.regexp && info.title.match(new RegExp(regexp))) {
          console.log("fg-window", "try focus")
          chrome.windows.update(tab.windowId, { focused: true })
        }
      }
    })
  }
})
